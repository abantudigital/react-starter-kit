import { css } from 'styled-components';

// import { calculateRem } from 'utils/calculateRem';
// import breakpoint from 'styled-components-breakpoint';

const inputFieldStyles = css`
  &::after {
    background-color: red !important;
  }
`;

export default inputFieldStyles;
