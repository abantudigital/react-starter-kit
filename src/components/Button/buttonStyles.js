import { css } from 'styled-components';
// import { calculateRem } from 'utils/calculateRem';
// import breakpoint from 'styled-components-breakpoint';

const buttonStyles = css`
  width: 100%;
  padding: 1rem;
  border: none;
  outline: none;
  cursor: pointer;
`;

export default buttonStyles;
